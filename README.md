## Monitoring Infiniband Device Statistics with Zabbix LLD and User Parameters
Files in `userparameters/` in this repo translate to files in `/etc/zabbix/zabbix_agentd.d`.

Files in `templates/` in this repo can be imported into Zabbix.